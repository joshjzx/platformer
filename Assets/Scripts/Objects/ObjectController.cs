﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectController : MonoBehaviour
{

    GameManager manager;
    PlayerController player;
    private void Start()
    {
         manager = GetComponent<GameManager>();
         player = GetComponent<PlayerController>();
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        // Doors and Such

       if (col.gameObject.tag.Equals("Player"))
        {

            player = col.GetComponent<PlayerController>();

            if (gameObject.name == "RedDoor")
            {
                if (player.hasRedKey)
                {
                    gameObject.SetActive(false);
                    player.hasRedKey = false;
                }
            }
            else if (gameObject.name == "BlueDoor")
            {
                if (player.hasBlueKey)
                {
                    gameObject.SetActive(false);
                    player.hasBlueKey = false;
                }
            }
            else if (gameObject.name == "GreenDoor")
            {
                if(player.hasGreenKey)
                {
                    gameObject.SetActive(false);
                    player.hasGreenKey = false;
                }
            }
            else if (gameObject.name == "Computer")
            {
                if (player.hasFloppy)
                {
                    /*
                    need to add all the lasers in a room so they can be deactivated by the computer
                    List<> would work
                    */
                    Debug.Log("Computer has disabled Lasers in this area");
                    player.hasFloppy = false;
                }
            }

            // Keys and Items

            else if (gameObject.name == "RedKey")
            {
                player.hasRedKey = true;
                gameObject.SetActive(false);
            }
            else if (gameObject.name == "BlueKey")
            {
                player.hasBlueKey = true;
                gameObject.SetActive(false);
            }
            else if (gameObject.name == "GreenKey")
            {
                player.hasGreenKey = true;
                gameObject.SetActive(false);
            }
            else if (gameObject.name == "Ammo")
            {
                player.ammoCount += 5;
                gameObject.SetActive(false);
            }
            else if (gameObject.name == "TNT")
            {
                player.hasTnt = true;
                gameObject.SetActive(false);
            }
            else if (gameObject.name == "FloppyDisk")
            {
                player.hasFloppy = true;
                gameObject.SetActive(false);
            }
        }
    }
}
