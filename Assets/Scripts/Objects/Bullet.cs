﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float speed;
    public Rigidbody2D rb;
    public int damage;

    // Start is called before the first frame update
    void Start()
    {
        rb.velocity = transform.right * speed;
    }

    private void OnTriggerEnter2D(Collider2D hitInfo)
    {
        if (hitInfo.gameObject.tag.Equals("Enemy"))
        {
            EnemyManager enemy = hitInfo.transform.GetComponent<EnemyManager>();
            if (enemy != null)
            {
                Debug.Log(hitInfo.gameObject.name);
                enemy.TakeDamage(damage, hitInfo.transform.parent);
            }
        }

        if (hitInfo.gameObject.tag.Equals("Player"))
        {
            PlayerController player = hitInfo.transform.GetComponent<PlayerController>();
            if (player != null)
            {
                Debug.Log("Player Hit");
                player.TakeDamage(damage);
            }
        }   
            Destroy(this.gameObject);
        }

}