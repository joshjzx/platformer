﻿using UnityEngine;

public class Weapon : MonoBehaviour
{

    public Transform firePointLeft;
    public Transform firePointRight;
    public GameObject bulletPrefab;

    [SerializeField]
    PlayerController player;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire2"))
        {
            Shoot();
        }
    }

    void Shoot()
    {
        if (player.ammoCount > 0)
        {

            float thrust = 5f;
            if (player.faceRight)
            {
                GameObject bullet = Instantiate(bulletPrefab, firePointRight.position, firePointRight.rotation);
                bullet.GetComponent<Rigidbody2D>().AddForce(transform.forward * thrust * 5f * Time.deltaTime);
            }
            else if (player.faceLeft)
            {
                GameObject bullet = Instantiate(bulletPrefab, firePointLeft.position, firePointLeft.rotation);
                bullet.GetComponent<Rigidbody2D>().AddForce(transform.forward * thrust * 5f * Time.deltaTime);
            }

            player.ammoCount = player.ammoCount - 1;
        }
    }
}
