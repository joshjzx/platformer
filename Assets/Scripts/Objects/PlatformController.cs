﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour
{
    [Header("Colliders")]
    [SerializeField]
    BoxCollider2D playerCollider;
    [SerializeField]
    PolygonCollider2D boundsCollider;

    [Header("Platform Settings")]
    [SerializeField]
    float moveSpeed;

    int direction = 1;
    Vector2 movement;

    [SerializeField]
    bool collided = false;
    
    private void OnTriggerEnter2D(Collider2D other)
    {
        other = GetComponentInChildren<PolygonCollider2D>();

        if (other.tag == "boundsCollider")
        {
            collided = !collided;
        }
    }
        
    void FixedUpdate()
    {

        if (!collided)
        {
            movement = Vector2.right* direction *moveSpeed * Time.deltaTime;
            transform.Translate(movement); 
        }
        else
        {
            movement = Vector2.left * direction * moveSpeed * Time.deltaTime;
            transform.Translate(movement);
        }
    }
}
