﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour
{
    [SerializeField]
    EnemyValues enemyValues;

    public int currentHealth;

    public void ChangeHealth(int current, int value, GameObject go)
    {
           currentHealth = current;
           currentHealth -= value;

                if (currentHealth - value < 0)
                {
                    currentHealth = 0;
                }
    }
    
    public int ReturnScore(GameObject go)
    {
       return EnemyValues.Score;
    }

    public int CurrentHealth(GameObject go)
    {
        GameObject v = go.gameObject;
        int health;

        if (go != null)
        {
            health = v.GetComponent<EnemyHealth>().currentHealth;
        }
        else
        {
            Debug.LogError("Attention no GameObject passed to CurrentHealth()");
            return -1;
        }
        return health;
    }
       
    public EnemyValues EnemyValues
    {
        get { return enemyValues; }
        set { enemyValues = value; }
    }
}
