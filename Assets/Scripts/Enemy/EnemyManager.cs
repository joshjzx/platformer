﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    SpawnManager spawner;
    EnemyController enemy;
    EnemyHealth enemyHealth;

    private void Awake()
    {
        spawner = FindObjectOfType<SpawnManager>();
        enemy = FindObjectOfType<EnemyController>();
        enemyHealth = FindObjectOfType<SpawnManager>().GetComponent<EnemyHealth>();

        if (enemy == null)
        {
            Debug.LogError("No EnemyController Object found");
        }

        if (enemyHealth == null)
        {
            Debug.LogError("No EnemyHealth Object found");
        }
    }

    public void TakeDamage(int damage, Transform go)
    {      
        Vector3 previousPos = go.transform.position;
        int health = enemyHealth.CurrentHealth(go.transform.gameObject);
        enemyHealth.ChangeHealth(health, damage, go.gameObject);
        health = enemyHealth.currentHealth;
        spawner.RemoveChild(go.transform);
        spawner.SpawnPrefab(health, go.gameObject, go.transform, previousPos);
    }
}
