﻿using UnityEngine;

public class EnemyDead : MonoBehaviour
{
    EnemyHealth enemy;

    private void Awake()
    {
      
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        GameObject v = this.transform.parent.gameObject;
        enemy = v.GetComponent<EnemyHealth>();

        if (col.gameObject.tag.Equals("Player"))
        {
            FindObjectOfType<GameManager>().AddScore(enemy.ReturnScore(v));
            Destroy(this.gameObject);
        }
    }

     
}
