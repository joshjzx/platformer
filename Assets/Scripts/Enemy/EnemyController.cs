﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [Header("Enemy Constants")]
    [SerializeField]
    private Animator anim;

    [SerializeField]
    private GameObject bulletprefab;

    [SerializeField]
    public Transform projectileSpawnPoint;

    [SerializeField]
    private Rigidbody2D rb;  // player rigidbody2D

    [SerializeField]
    private Collider2D col;  // player collider2D used to interact with Objects (Box Collider at this time)

    void Shoot()
    {

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        Shoot();
    }


}
