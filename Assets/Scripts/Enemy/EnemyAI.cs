﻿using UnityEngine;

public class EnemyAI : MonoBehaviour
{
    public float walkSpeed = 1.5f;      // Walkspeed
    public float groundDistance;        // 0.1f works best at the moment    
    public float wallDistance;          // 0.1f works best at the moment    
    public bool movingRight = true;
    public Transform groundDetection;
    public Transform wallDetection;
    public bool hitWall = false;
    public bool grounded = false;

    private void Update()
    {
        groundHit();
        wallHit();
    }

    void FixedUpdate()
    {
        transform.Translate(Vector2.right * walkSpeed * Time.deltaTime);

            if (grounded == false || hitWall == true)
            {
                if (movingRight == true)
                {
                    transform.eulerAngles = new Vector3(0, -180, 0);
                    movingRight = !movingRight;
                    hitWall = false;
                    grounded = true;
                } else
                {
                    transform.eulerAngles = new Vector3(0, 0, 0);
                    movingRight = !movingRight;
                    hitWall = false;
                    grounded = true;
                }
            }
    }

    RaycastHit2D groundHit()
    {

        Vector3 direction;
        Vector3 startPosition;
        Vector3 endPosition;
        RaycastHit2D groundInfo;

        if (movingRight == true)
        {
            direction = new Vector3(30, -30, 0);
            startPosition = transform.position + (0.01f * direction);
            endPosition = transform.forward + (0.1f * direction);

            // "13" - Ground // indicates a layer we want to raycast on and exclude others
            groundInfo = Physics2D.Raycast(startPosition, endPosition, groundDistance, 13); 
            
            Collider2D col = groundInfo.collider;

            if (col == null)
            {
                grounded = false;
            }
            else
            {
                grounded = true;
            }
            return groundInfo;
        }
        else
        {
            direction = new Vector3(-30, -30, 0);
            startPosition = transform.position + (0.01f * direction);
            endPosition = transform.forward + (0.1f * direction);

            // "13" - Ground // indicates a layer we want to raycast on and exclude others
            groundInfo = Physics2D.Raycast(startPosition, endPosition, 0.1f, 13);
           
            Collider2D col = groundInfo.collider;

            if (col == null)
            {
                grounded = false;
            }
            else
            {
                grounded = true;
            }
            return groundInfo;
        }
    }
    RaycastHit2D wallHit()
    {
        RaycastHit2D wallInfo = Physics2D.Raycast(wallDetection.position, Vector2.right, wallDistance);
        if (wallInfo)
        {
            hitWall = true;
        }
        return wallInfo;
    }
}