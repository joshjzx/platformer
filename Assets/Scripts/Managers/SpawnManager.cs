﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    SpawnPointManager spawnPointManager;

    EnemyHealth enemyHealth;

    [SerializeField]
    public GameObject[] enemyPrefabs;

    private void Start()
    {
       
    }

    public void SpawnPrefab(int health, GameObject gob ,Transform transform, Vector3 position)
    {
        GameObject v = gob.gameObject;
        Transform lastPos = transform;
        lastPos.position = position;
        Debug.Log(health);
        enemyHealth = v.GetComponent<EnemyHealth>();
        enemyHealth.currentHealth = health;

        if (health >= 5)
        {
           GameObject go = Instantiate(enemyPrefabs[0], lastPos.position, lastPos.rotation) as GameObject;
            go.transform.parent = lastPos;
          //  go.transform.position = lastPos.position;
        }

        if (health == 4)
        {
            GameObject go = Instantiate(enemyPrefabs[1], lastPos.position, lastPos.rotation) as GameObject;
            go.transform.parent = lastPos;
            //go.transform.position = lastPos.position;
        }

        if (health == 3)
        {
            GameObject go = Instantiate(enemyPrefabs[2], lastPos.position, lastPos.rotation) as GameObject;
            go.transform.parent = lastPos;
            //go.transform.position = lastPos.position;
        }

        if (health == 2)
        {
            lastPos.position = position;
            GameObject go = Instantiate(enemyPrefabs[3], lastPos.position, lastPos.rotation) as GameObject;
            go.transform.parent = lastPos;
            go.transform.position = lastPos.position;
        }

        if (health == 1)
        {
            GameObject go = Instantiate(enemyPrefabs[4], lastPos.position, lastPos.rotation) as GameObject;
            go.transform.parent = lastPos;
            //go.transform.position = lastPos.position;
        }

        if (health <= 0)
        {
            Die(transform, position);
        }
    }

    public void Die(Transform transform, Vector3 position)
    {
        Transform lastPos = transform;
        Vector3 pos = position;

        transform.eulerAngles = new Vector3(0, 0, 0);
        GameObject go = Instantiate(enemyPrefabs[5], position, lastPos.rotation) as GameObject;  //prefab 5 is the Enemydead Prefab
        go.transform.parent = lastPos;
        go.transform.position = lastPos.position;
    }

    public void RemoveChild(Transform transform)
    {
        Transform lastPos = (transform);

        Debug.Log(lastPos.name);

        for (var i = lastPos.childCount; i-- > 0;)
        {
            Destroy(transform.GetChild(0).gameObject);
        }
    }
}
