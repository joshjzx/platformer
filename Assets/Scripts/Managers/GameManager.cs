﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject spawnPoint;

    public GameObject playerPrefab;

    [SerializeField]
    Cinemachine.CinemachineVirtualCamera cam;

    GameObject player;

    EnemyController enemy;

    [SerializeField]
    int score = 0;

    private void Start()
    {
        Spawn();
        cam.Follow = GameObject.FindGameObjectWithTag("Player").transform;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("Test");
        }
    }

    public void Spawn()
    {
        Instantiate(playerPrefab, (spawnPoint.transform.position), spawnPoint.transform.rotation);
    }

    public int AddScore(int amount)
    {
        score += amount;

        return score;
    }
}
