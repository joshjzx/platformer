﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointManager : MonoBehaviour
{
    [SerializeField]
    EnemySpawnPoint[] spawnPoints;

    EnemyHealth enemyHealth;

    EnemyValues enemyValues;

    private void Start()
    {
        GameObject[] array = new GameObject[spawnPoints.Length];
        int x = 0;

        foreach (EnemySpawnPoint child in spawnPoints)
        {
            array[x] = child.gameObject;
            x++;
        }

        for (int i = 0; i < array.Length; i++)
        {

            var spawn = array[i].GetComponent<SpawnManager>();
            enemyValues = spawn.GetComponent<EnemyHealth>().EnemyValues;
            //Debug.Log(spawn.gameObject.name);
            spawn.SpawnPrefab(enemyValues.MaxHealth, spawn.gameObject ,spawn.gameObject.transform, spawn.transform.position);
        }
    }
}
