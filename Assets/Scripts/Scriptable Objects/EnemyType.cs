﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum enemyTypes
{
    enemyGrey,
    enemyBlue,
    enemyNinja,
    enemyRed,
    enemyWhite,
    enemyDog,
    enemyRobot
};

[CreateAssetMenu]
public class EnemyType : ScriptableObject
{
    public enemyTypes[] type;
    public int[] startingHealth;

    private Dictionary<enemyTypes, int> dic;

    void Awake()
    {
        int length = type.Length;
        this.dic = new Dictionary<enemyTypes, int>(length);
        for (int i = 0; i < length; i++)
        {
            this.dic.Add(type[i], startingHealth[i]);
        }
    }

}