﻿using UnityEngine;

[CreateAssetMenu]
public class EnemyValues : ScriptableObject
{
    [SerializeField]
    int maxHealth;

    [SerializeField]
    int score;

    public int MaxHealth
    {
        get
        {
            return maxHealth;
        }
    }

    public int Score
    {
        get
        {
            score = MaxHealth * 100;
            return score;
        }
    }
}
