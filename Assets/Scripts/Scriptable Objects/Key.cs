﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum KeyType
{
    greenKey,
    blueKey,
    redKey
}

[CreateAssetMenu]
public class Key : ScriptableObject
{
    public KeyType keyType;
    public Sprite itemSprite;
    public string itemDescription;
    public bool isKey;

}