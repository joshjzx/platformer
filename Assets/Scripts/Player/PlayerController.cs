﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    GameManager gameManager;

    [Header("Movement Items")]

    [SerializeField]
    float movementSpeed; // movement speed value

    [SerializeField]
    [Range(0, 1)] public float sliding;

    [SerializeField]
    float jumpForce;

    [Header("Character Constants")]

    [SerializeField]
    private Animator anim;

    [SerializeField]
    private GameObject bulletprefab;

    [SerializeField]
    private Rigidbody2D rb;  // player rigidbody2D

    [SerializeField]
    private Collider2D col;  // player collider2D used to interact with Objects (Box Collider at this time)

    bool grounded;
    bool onPlatform = false;
    public bool hasTnt = false;
    public bool hasFloppy = false;
    public bool hasBlueKey = false;
    public bool hasGreenKey = false;
    public bool hasRedKey = false;
    public int ammoCount;
    public int health;
    public bool faceLeft = false;
    public bool faceRight = true;
    public bool isJumping = false;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();  // grab the rigidbody
        anim = GetComponent<Animator>();  // grab the animator
        gameManager = GetComponent<GameManager>(); // grab the Game Manager
    }

    bool IsGrounded()
    {
        Bounds bounds = GetComponent<Collider2D>().bounds;
        float range = bounds.size.y * 0.1f;
        Vector2 v = new Vector2(bounds.center.x, bounds.min.y - range);
        RaycastHit2D hit = Physics2D.Linecast(v, bounds.center);

        if (Input.GetButton("Jump"))
        {
            isJumping = true;
        }
        else isJumping = false;

        //  Set this check that we dont interact with the enemies colliders as this caused  
        //  an increase to the jump height that was not correct.
        if (!hit.collider.gameObject.tag.Equals("Enemy"))   
        {
            return (hit.collider.gameObject != gameObject);
        }
        else return false;
    }

    public void Die()
    {
        if (health <= 0)
        {
            Destroy(this.gameObject);
        }    
    }

        public void TakeDamage(int damage)
    {
        health = health - damage;
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name.Equals("playerCollider"))
        {
            this.transform.parent = col.transform;
        }

        if (col.gameObject.tag.Equals("NoGo"))
        {
            Destroy(this.gameObject);
        }

        if (col.gameObject.tag.Equals("Enemy") || col.gameObject.tag.Equals("Bullet"))
        {
            health = health - 1;
        }

        if (col.gameObject.name.Equals("TNT"))
        {
            hasTnt = !hasTnt;
        }

        if (col.gameObject.name.Equals("FloppyDisk"))
        {
            hasFloppy = !hasFloppy;
        }

        if (col.gameObject.name.Equals("BlueKey"))
        {
            hasBlueKey = !hasBlueKey;
        }

        if (col.gameObject.name.Equals("GreenKey"))
        {
            hasGreenKey = !hasGreenKey;
        }

        if (col.gameObject.name.Equals("RedKey"))
        {
            hasRedKey = !hasRedKey;
        }
    }

    private void OnCollisionExit2D(Collision2D col)
    {
        if (col.gameObject.tag.Equals("playerCollider"))
        {
            this.transform.parent = null;
        }
    }

    void FixedUpdate()
    {
        float h = Input.GetAxisRaw("Horizontal");
        Vector2 v = rb.velocity;
        bool grounded = IsGrounded();

        if (h != 0)
        {
            anim.SetBool("iswalking", true);

            anim.SetFloat("moveX", Input.GetAxisRaw("Horizontal"));

            if ((Input.GetAxisRaw("Horizontal")) < 0)
            {
                faceLeft = true;
                faceRight = false;
                anim.SetBool("walkLeft", true);
                anim.SetBool("walkRight", false);
            }
            else
            {
                faceLeft = false;
                faceRight = true;
                anim.SetBool("walkLeft", false);
                anim.SetBool("walkRight", true);
            }
        }
        else
        {
            anim.SetBool("iswalking", false);
        }

        if (h != 0 && !onPlatform)
        {
            rb.velocity = new Vector2(h * movementSpeed, v.y);
        }
        else
        {
            rb.velocity = new Vector2(v.x * sliding, v.y);
        }

        if (isJumping && grounded)
        {
            rb.AddForce(new Vector2(GetComponent<Rigidbody2D>().velocity.x, jumpForce));
            anim.SetBool("isjumping", true);
        }
        else if (!isJumping && grounded)
        {
            anim.SetBool("isjumping", false);
        }
    }

    private void Update()
    {
        Die();
    }
}
